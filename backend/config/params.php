<?php
return [
    'language' => require(__DIR__ . '/language.php'),
    'country' => require(__DIR__ . '/country.php'),
    'adminEmail' => 'admin@example.com',
    'default_page_size' => 10,
    'default_save_version_num' => 5,
    'allow_img_format' => ['png', 'jpeg', 'jpg', 'gif', 'avg', 'svg'],
    'default_system' => 'T9N',

    'errCode' => [
        'dataValidateErr' => 0,
        'logicErr' => 2,
    ],
    'errMsg' => [
        'dataValidateErr' => 'invalid data',
        'logicErr' => 'not conform to design logic',
    ],
];
