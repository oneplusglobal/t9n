<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'base/error',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                $requestHeader = Yii::$app->request->getHeaders();
                $originUrl = isset($requestHeader['origin']) ? $requestHeader['origin'] : isset($requestHeader['refer']) ? $requestHeader['origin'] : '*';

                $response->getHeaders()
                    ->set('Access-Control-Allow-Credentials', 'true')
                    ->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, openToken')
                    ->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
                    ->set('Access-Control-Allow-Origin', $originUrl)
                    ->set('Allow', 'POST, GET, PUT, DELETE, OPTIONS');
            },
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
           // 'enableStrictParsing' => true,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'locale',
                    'extraPatterns' => [
                        'GET parent' => 'parent',
                        'GET base' => 'base',
                        'GET all' => 'all',
                    ],
                    'pluralize'=>false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'source',
                    'extraPatterns' => [
                        'GET tag' => 'tag',
                    ],
                    'pluralize'=>false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'translate',
                    'extraPatterns' => [
                        'POST publish' => 'publish',
                    ],
                    'pluralize'=>false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'dashboard',
                    'pluralize'=>false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'user',
                    'extraPatterns' => [
                        'GET getuser' => 'getuser',
                        'POST login'  => 'login',
                        'GET logout'  => 'logout',
                    ],
                    'pluralize'=>false,
                ],
            ],
        ],
    ],
    'params' => $params,
];
