<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/1/22
 * Time: 12:16
 */
namespace backend\models;
use Yii;
use yii\rest\Serializer as SysSerializer;

class Serializer extends SysSerializer
{
    /**
     * Serializes a data provider.
     * @param DataProviderInterface $dataProvider
     * @return array the array representation of the data provider.
     */
    protected function serializeDataProvider($dataProvider)
    {
        $models = $this->serializeModels($dataProvider->getModels());
        $pagination = $dataProvider->getPagination();

        if ($this->request->getIsHead()) {
            return null;
        } elseif ($this->collectionEnvelope === null) {
            return $models;
        } else {
            $result = [
                $this->collectionEnvelope => $models,
            ];
            if ($pagination !== false) {
                return array_merge($result, $this->serializePagination($pagination));
            } else {
                return $result;
            }
        }
    }

    protected function serializePagination($pagination)
    {
        return [
            'page' => [
                'totalRecord' => $pagination->totalCount,
                'totalPage' => $pagination->getPageCount(),
                'currentPage' => $pagination->getPage() + 1,
                'pageSize' => $pagination->getPageSize(),
            ],
        ];
    }
}