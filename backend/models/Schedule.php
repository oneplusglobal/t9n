<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property string $tag
 * @property integer $source_language
 * @property string $target_languages
 * @property integer $translator
 * @property integer $status
 * @property integer $create_time
 * @property integer $creator_id
 * @property integer $modify_time
 * @property integer $modifier_id
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_language', 'translator', 'status', 'create_time', 'creator_id', 'modify_time', 'modifier_id'], 'integer'],
            [['tag'], 'string', 'max' => 20],
            [['target_languages'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag' => 'Tag',
            'source_language' => 'Source Language',
            'target_languages' => 'Target Languages',
            'translator' => 'Translator',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'creator_id' => 'Creator ID',
            'modify_time' => 'Modify Time',
            'modifier_id' => 'Modifier ID',
        ];
    }
}
