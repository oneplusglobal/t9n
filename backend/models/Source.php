<?php

namespace backend\models;

use Yii;
use backend\models\Locale;
use backend\models\Translate;

/**
 * This is the model class for table "source".
 *
 * @property integer $id
 * @property string $tag
 * @property string $content
 * @property integer $language_id
 * @property string $system
 * @property integer $type
 * @property integer $status
 * @property integer $create_time
 * @property integer $creator_id
 * @property integer $modify_time
 * @property integer $modifier_id
 */
class Source extends \yii\db\ActiveRecord
{
    const TYPE_CONTENT = 1;
    const TYPE_IMAGE   = 2;
    const TYPE_UI      = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag', 'content', 'type', 'create_time', 'modify_time'], 'required'],
            [['content'], 'string'],
            ['content', function($attribute, $params){
                if(!filter_var($this->$attribute, FILTER_VALIDATE_URL) || !in_array(substr($this->$attribute, strrpos($this->$attribute, '.')+1), Yii::$app->params['allow_img_format'])){
                    $this->addError($attribute, 'This url is invalid.');
                }
            }, 'when' => function($model){
                return $model->type == Source::TYPE_IMAGE;
            }],
            ['tag', function($attribute, $params){
                if(!preg_match("/^[a-f0-9]{32}$/", $this->$attribute)){
                    $this->addError($attribute, 'This tag is invalid.');
                }
            }],
            [['language_id', 'type', 'status', 'create_time', 'creator_id', 'modify_time', 'modifier_id'], 'integer'],
            [['tag', 'system'], 'string', 'max' => 50],
            ['tag', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag' => 'Tag',
            'content' => 'Content',
            'language_id' => 'Language ID',
            'system' => 'System',
            'type' => 'Type',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'creator_id' => 'Creator ID',
            'modify_time' => 'Modify Time',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public static function getAttributeName(){
        return [
            'id',
            'tag',
            'content',
            'language_id',
            'system',
            'type',
            'status',
            'create_time',
            'creator_id',
            'modify_time',
            'modifier_id',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['language'] = function(){
            return Locale::getOnlyLocale($this->language_id);
        };
        $fields['locales'] = function(){
            return Translate::getAllVersionLocale($this->tag);
        };

        $fields['creator'] = function(){
            return 'T9N';
        };

        unset($fields['creator_id'], $fields['modifier_id']);

        return $fields;
    }

    public static function getStringCountByType($type){
        return self::find()->where(['type' => $type, 'status' => Source::STATUS_ACTIVE])->count();
    }

    public static function getRecentAdd(){
        return self::find()->orderBy(['create_time' => SORT_DESC])->limit(5)->all();
    }

    public static function changeStatus($id){
        $model = self::findOne($id);
        $model->status = self::STATUS_EXPIRE;

        return $model->save();
    }

    public static function getType($tag){
        $model = self::findOne(['tag' => $tag]);
        return isset($model->type) ? $model->type : '';
    }

    public static function haveTag($tag){
        $model = self::findOne(['tag' => $tag]);
        return ($model) ? true : false;
    }

    public static function getTag(){
        return md5("T9N".time().rand());
    }
}
