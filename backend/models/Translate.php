<?php

namespace backend\models;

use Yii;
use backend\models\Locale;

/**
 * This is the model class for table "translate".
 *
 * @property integer $id
 * @property string $tag
 * @property integer $language_id
 * @property string $translate
 * @property integer $publish
 * @property integer $create_time
 * @property integer $creator_id
 * @property integer $modify_time
 * @property integer $modifier_id
 */
class Translate extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISH = 1;
    const STATUS_UNPUBLISH = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag', 'publish', 'translate', 'create_time', 'modify_time'], 'required'],
            [['language_id', 'publish', 'create_time', 'creator_id', 'modify_time', 'modifier_id'], 'integer'],
            [['translate'], 'string'],
            ['translate', function($attribute, $params){
                if(!filter_var($this->$attribute, FILTER_VALIDATE_URL) || !in_array(substr($this->$attribute, strrpos($this->$attribute, '.')+1), Yii::$app->params['allow_img_format'])){
                    $this->addError($attribute, 'This url is invalid.');
                }
            }, 'when' => function($model){
                return Source::getType($model->tag) == Source::TYPE_IMAGE;
            }],
            ['tag', function($attribute, $params){
                if(!Source::haveTag($this->$attribute)){
                    $this->addError($attribute, 'This tag is invalid.');
                }
            }],
            [['tag'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag' => 'Tag',
            'language_id' => 'Language ID',
            'translate' => 'Translate',
            'publish' => 'Publish',
            'create_time' => 'Create Time',
            'creator_id' => 'Creator ID',
            'modify_time' => 'Modify Time',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['creator'] = function(){
            return 'T9N';
        };

        unset($fields['creator_id'], $fields['modifier_id']);

        return $fields;
    }

    public static function getAllVersionLocale($tag){
        $allVersion = self::getAllVersion($tag);

        $locales = [];
        foreach($allVersion as $v){
            $language = Locale::getOnlyLocale($v['language_id']);
            if(!in_array($language, $locales) && !empty($language)){
                $locales[] = $language;
            }
        }
        array_filter($locales);
        asort($locales);
        return implode(", ", $locales);
    }

    public static function getAllVersion($tag){
        $result = self::find()->where(['tag' => $tag])->orderBy(['create_time' => SORT_DESC])->all();
        return $result;
    }

    public static function getVersionCount($tag, $language_id){
        return self::find()->where(['tag' => $tag, 'language_id' => $language_id])->count();
    }

    public static function isUiHaveThisLangVersion($tag, $language_id){
        return self::find()->where(['tag' => $tag, 'language_id' => $language_id])->count();
    }

    public static function getOldest($tag, $language_id){
        $result = self::find()->where(['tag' => $tag, 'language_id' => $language_id])->orderBy('create_time')->all();
        $id = $result[0]['id'];
        $model = self::findOne($id);
        return $model;
    }

    public static function deleteOldest($model){
        return $model->delete();
    }

    public static function getPublishId($tag, $language_id){
        $model = self::find()->where(['tag' => $tag, 'language_id' => $language_id, 'publish' => Translate::STATUS_PUBLISH])->one();
        return isset($model->id) ? $model->id : '';
    }

    public static function newPublish($id){
        $model = self::findOne($id);

        if($model){
            $oldId = self::getPublishId($model->tag, $model->language_id);
            if(!empty($oldId)){
                self::changePublish($oldId);
            }
            self::changePublish($id);
            return true;
        }
        return false;
    }

    public static function changePublish($id){
        $model = self::findOne($id);
        $model->publish = ($model->publish == self::STATUS_PUBLISH) ? self::STATUS_UNPUBLISH : self::STATUS_PUBLISH;
        return $model->save();
    }

    public static function getLatestUpdateTime(){
        $model = self::find()->where(['publish' => Translate::STATUS_PUBLISH])->orderBy(['modify_time' => SORT_DESC])->one();
        return $model->modify_time;
    }

    public static function sdkGetAll(){
        $model = self::find()->select(['tag', 'language_id', 'translate'])->where(['publish' => self::STATUS_PUBLISH])->all();
        return $model;
    }

    public static function getByTagAndLocale($tag, $locale){
        $model = self::find()->where(['tag' => $tag, 'language_id' => $locale, 'publish' => self::STATUS_PUBLISH])->one();
        return $model;
    }
}
