<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $schedule_id
 * @property integer $language
 * @property string $translate
 * @property integer $create_time
 * @property integer $creator_id
 * @property integer $modify_time
 * @property integer $modifier_id
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_id', 'language', 'create_time', 'creator_id', 'modify_time', 'modifier_id'], 'integer'],
            [['translate'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Schedule ID',
            'language' => 'Language',
            'translate' => 'Translate',
            'create_time' => 'Create Time',
            'creator_id' => 'Creator ID',
            'modify_time' => 'Modify Time',
            'modifier_id' => 'Modifier ID',
        ];
    }
}
