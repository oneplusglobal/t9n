<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "locale".
 *
 * @property integer $id
 * @property string $country
 * @property string $language
 * @property string $locale
 * @property string $time_format
 * @property string $short_date_format
 * @property string $long_date_format
 * @property string $weight_units
 * @property string $length_units
 * @property string $currency
 * @property string $decimal_symbol
 * @property integer $fallback
 * @property integer $create_time
 * @property integer $creator_id
 * @property integer $modify_time
 * @property integer $modifier_id
 */
class Locale extends \yii\db\ActiveRecord
{
    const DEFAULT_SOURCE_LANGUAGE = 1;

    //default parent as fallback should be en_us which have 1 as primary key
    const DEFAULT_FALLBACK = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'language', 'locale'], 'required'],
            [['language'], 'string', 'max' => 3],
            [['country'], 'string', 'max' => 2],
            [['locale'], 'string', 'max' => 6],
            [['fallback', 'create_time', 'creator_id', 'modify_time', 'modifier_id'], 'integer'],
            [['create_time', 'modify_time'], 'required'],
            [['currency'], 'string', 'max' => 50],
            [['time_format', 'short_date_format', 'long_date_format', 'weight_units', 'length_units'], 'string', 'max' => 20],
            [['decimal_symbol'], 'string', 'max' => 10],
            ['locale', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'language' => 'Language',
            'locale' => 'Locale',
            'time_format' => 'Time Format',
            'short_date_format' => 'Short Date Format',
            'long_date_format' => 'Long Date Format',
            'weight_units' => 'Weight Units',
            'length_units' => 'Length Units',
            'currency' => 'Currency',
            'decimal_symbol' => 'Decimal Symbol',
            'fallback' => 'Fallback',
            'create_time' => 'Create Time',
            'creator_id' => 'Creator ID',
            'modify_time' => 'Modify Time',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public static function getAttributeName(){
        return [
            'id',
            'country',
            'language',
            'locale',
            'time_format',
            'short_date_format',
            'long_date_format',
            'weight_units',
            'length_units',
            'currency',
            'decimal_symbol',
            'fallback',
            'create_time',
            'creator_id',
            'modify_time',
            'modifier_id',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'country',
            'language',
            'locale',
            'create_time',
            'modify_time',
            'parent_id' => 'fallback',
            'parent' => function(){
                return $this->getOnlyLocale($this->fallback);
            }
        ];
    }

    public static function getAllParent(){
        $result = self::find()->select(['id','locale'])->where("fallback = :default", [':default'=>self::DEFAULT_FALLBACK])->all();

        $parent = [];
        foreach($result as $v){
            $parent[] = [
                'id'     => $v->id,
                'locale' => $v->locale,
            ];
        }
        return $parent;
    }

    public static function getAllLocale(){
        $data = self::find()->select(['id','locale'])->all();

        $result = [];
        foreach($data as $v){
            $result[] = [
                'id'     => $v->id,
                'locale' => $v->locale,
            ];
        }

        usort($result, function($a, $b){
            return ($a['locale'] > $b['locale']) ? 1 : -1;
        });
        return $result;
    }

    public static function getBaseCountry(){
        $countries = [];
        foreach(Yii::$app->params['country'] as $k => $v){
            $countries[] = [
                'abbr' => $k,
                'name' => $v,
            ];
        }
        return $countries;
    }

    public static function getBaseLanguage(){
        $languages = [];
        foreach(Yii::$app->params['language'] as $k => $v){
            $languages[] = [
                'abbr' => $k,
                'name' => $v,
            ];
        }
        return $languages;
    }

    public static function getOnlyLocale($id){
        $model = self::findOne(['id' => $id]);
        return isset($model->locale) ? $model->locale : '';
        //return self::find()->select('locale')->where("id = :id", [':id'=>$id])->one()->locale;
    }

    public static function sdkGetAll(){
        $model = self::find()->all();
        return $model;
    }
}
