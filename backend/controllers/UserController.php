<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/1/20
 * Time: 14:42
 */
namespace backend\controllers;

use backend\controllers\BaseController;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\base\Model;
use Yii;

class UserController extends BaseController
{
    public $modelClass = 'backend\models\User';

    public $scenario = Model::SCENARIO_DEFAULT;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view'], $actions['index']);
        return $actions;
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),[
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'verbs' => ['OPTIONS']
                    ],
                    [
                        'actions' => ['login', 'getuser', 'logout'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    public function actionLogin(){
        if (!\Yii::$app->user->isGuest) {
            return;
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            return;
        } else {
            $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
            return;
        }
    }

    public function actionLogout(){
        if (!\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        return;
    }

    public function actionGetuser(){
        if (\Yii::$app->user->isGuest) {
            $this->ret = Yii::$app->params['errCode']['logicErr'];
            $this->errMsg = Yii::$app->params['errMsg']['logicErr'];
            return;
        }

        return [
            'userId' => Yii::$app->user->identity->id,
            'username' => Yii::$app->user->identity->username,
        ];
    }
}