<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/1/20
 * Time: 14:42
 */
namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use backend\controllers\BaseController;
use backend\models\Locale;
use backend\models\Source;
use backend\models\Translate;

class SourceController extends BaseController
{
    public $modelClass = 'backend\models\Source';

    public $scenario = Model::SCENARIO_DEFAULT;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['index', 'create', 'view', 'delete', 'tag'],
                    'rules' => [
                        [
                            'allow' => true,
                            'verbs' => ['OPTIONS']
                        ],
                        [
                            'actions' => ['index', 'create', 'view', 'delete', 'tag'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                    'denyCallback' => function ($rule, $action) { throw new UnauthorizedHttpException('You are requesting with an invalid credential.');},
                ]
            ]
        );
        return $behaviors;
    }

    public function prepareDataProvider()
    {
        $perPage = Yii::$app->request->get('pageSize');
        $attr = Yii::$app->request->get('attr');
        $sort = Yii::$app->request->get('sort');
        $type = Yii::$app->request->get('type');
        if(empty($type)){
            $this->redirect(Url::toRoute(['index','type' => 1,  'pageSize' => empty($perPage) ? Yii::$app->params['default_page_size'] : $perPage], true));
        }

        $modelClass = $this->modelClass;
        $query = $modelClass::find()->where(['type' => $type, 'status' => $modelClass::STATUS_ACTIVE]);
        if(!empty($attr) && !empty($sort)){
            if(!in_array($attr, Source::getAttributeName())){
                $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
                $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
                return;
            }

            if($sort == 'desc'){
                $query = $query->orderBy([$attr => SORT_DESC]);
            }else{
                $query = $query->orderBy([$attr => SORT_ASC]);
            }
        }

        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => empty($perPage) ? Yii::$app->params['default_page_size'] : $perPage,
                'pageSizeParam' => 'pageSize',
                'pageParam' => 'currentPage',
            ],
            'query' => $query,
        ]);
    }

    public function actionCreate(){
        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->system = Yii::$app->params['default_system'];
        $model->language_id = Locale::DEFAULT_SOURCE_LANGUAGE;
        $model->status = $model::STATUS_ACTIVE;
        $model->create_time = $this->getTimeNow();
        $model->modify_time = $this->getTimeNow();
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            //$id = implode(',', array_values($model->getPrimaryKey(true)));
            //$response->getHeaders()->set('Location', Url::toRoute(["view", 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionView($id){
        //$id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;
        $source = $modelClass::findOne($id);
        $translation = Translate::getAllVersion($source->tag);

        $result = [
            'source' => $source,
            'translation' => $translation,
        ];
        return $result;
    }

    public function actionDelete($id){
        $modelClass = $this->modelClass;

        if ($modelClass::changeStatus($id) === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        return [];
    }

    public function actionTag(){
        return Source::getTag();
    }
}