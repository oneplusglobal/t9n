<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/1/22
 * Time: 16:26
 */
namespace backend\controllers;

use Yii;
use backend\models\Source;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\UnauthorizedHttpException;
use backend\controllers\BaseController;

class DashboardController extends BaseController
{
    public $modelClass = 'backend\models\Source';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view'], $actions['index']);
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['index'],
                    'rules' => [
                        [
                            'allow' => true,
                            'verbs' => ['OPTIONS']
                        ],
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                    'denyCallback' => function ($rule, $action) { throw new UnauthorizedHttpException('You are requesting with an invalid credential.');},
                ]
            ]
        );
        return $behaviors;
    }

    public function actionIndex(){
        return [
            'username' => 'T9N',
            'lastLoginTime' => $this->getTimeNow(),
            'stringCount' => $this->getStringCount(),
            'newStrings'  => $this->getRecentAdd(),
            'requestActivity' => $this->getSdkLog(),
        ];
    }

    protected function getStringCount(){
        return [
            'content' => Source::getStringCountByType(Source::TYPE_CONTENT),
            'image' => Source::getStringCountByType(Source::TYPE_IMAGE),
            'ui' => Source::getStringCountByType(Source::TYPE_UI),
        ];
    }

    protected function getRecentAdd(){
        return Source::getRecentAdd();
    }

    /**
     * @return array
     */
    protected function getSdkLog(){
        $sdkLog = __DIR__."/../runtime/logs/sdk.log";

        if(!file_exists($sdkLog)){
            return [];
        }

        $fp = fopen($sdkLog, 'r');

        $pos = -2; // Skip final new line character (Set to -1 if not present)

        $lines = array();
        $currentLine = '';
        $count = 0;

        while (-1 !== fseek($fp, $pos, SEEK_END)) {
            $char = fgetc($fp);
            if (PHP_EOL == $char) {
                $count++;
                $lineContent = substr($currentLine, strrpos($currentLine, ']')+2);
                $lineArray = array_combine(['client', 'contentNum', 'imageNum', 'uiNum', 'time'], explode(',', $lineContent));
                $lineArray['time'] *= 1000;
                $lines[] = $lineArray;
                $currentLine = '';
            } else {
                $currentLine = $char . $currentLine;
            }
            $pos--;

            if($count == 10) break;
        }

        return $lines;
    }
}