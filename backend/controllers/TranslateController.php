<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/1/20
 * Time: 14:42
 */
namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use backend\controllers\BaseController;
use backend\models\Locale;
use backend\models\Source;
use backend\models\Translate;

class TranslateController extends BaseController
{
    public $modelClass = 'backend\models\Translate';

    public $scenario = Model::SCENARIO_DEFAULT;

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['create', 'update', 'publish'],
                    'rules' => [
                        [
                            'allow' => true,
                            'verbs' => ['OPTIONS']
                        ],
                        [
                            'actions' => ['create', 'update', 'publish'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                    'denyCallback' => function ($rule, $action) { throw new UnauthorizedHttpException('You are requesting with an invalid credential.');},
                ]
            ]
        );
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view'], $actions['index']);
        return $actions;
    }

    public function actionCreate(){
        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $sourceType = Source::getType($model->tag);
        if(empty($sourceType)){
            $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
            return;
        }

        $deleteFlag = 0;
        $uiHaveFlag = 0;
        $publishFlag = 0;
        if($sourceType == Source::TYPE_CONTENT || $sourceType == Source::TYPE_IMAGE){
            $deleteFlag = Translate::getVersionCount($model->tag, $model->language_id) >= Yii::$app->params['default_save_version_num'];
        }elseif($sourceType == Source::TYPE_UI){
            $uiHaveFlag = Translate::getVersionCount($model->tag, $model->language_id);
            $model->publish = 0;
            $publishFlag = 1;
        }

        if($model->publish == 1){
            //make any new translation to be unpublished. use the publishFlag to get it published later
            $model->publish = 0;
            $publishFlag = 1;
        }

        if(!Translate::getVersionCount($model->tag, $model->language_id)){
            //if this translation is the first make it be published.
            $publishFlag = 1;
        }

        if($uiHaveFlag){
            $this->ret = Yii::$app->params['errCode']['logicErr'];
            $this->errMsg = Yii::$app->params['errMsg']['logicErr'];
            return;
        }

        $model->create_time = $this->getTimeNow();
        $model->modify_time = $this->getTimeNow();
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            //$id = implode(',', array_values($model->getPrimaryKey(true)));
            //$response->getHeaders()->set('Location', Url::toRoute(["view", 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        if($deleteFlag){
            $oldest = Translate::getOldest($model->tag, $model->language_id);

            //if the oldest one is published, we need to make sure the new one will be published while the oldest one is deleted.
            if($oldest->publish == 1){
                $publishFlag = 1;
            }
            Translate::deleteOldest($oldest);
        }

        if($publishFlag){
            //newPublish will reverse the publish status for this new one and the old published. This function will be also used in publish change interface.
            Translate::newPublish($model->id);
        }
        return Translate::findOne($model->id);
    }

    public function actionUpdate($id){
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        if(Source::getType($model->tag) != Source::TYPE_UI){
            $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
            return;
        }

        $model->scenario = $this->scenario;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->modify_time = $this->getTimeNow();

        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

    public function actionPublish(){
        $id = Yii::$app->getRequest()->getBodyParams('id');

        if(!Translate::newPublish($id)){
            $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
            return;
        }

        return;
    }

}