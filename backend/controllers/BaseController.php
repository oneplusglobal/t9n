<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2015/12/28
 * Time: 14:52
 */
namespace backend\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\base\Exception;
use yii\web\HttpException;
use yii\base\UserException;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use common\models\User;

class BaseController extends ActiveController
{
    public $ret = 1;
    public $errMsg = 'success';

    public $modelClass = '';

    public $serializer = [
        'class' => 'backend\models\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        /*
         * set response header to application/json
         */
        return ArrayHelper::merge(parent::behaviors(), [
            'contentNegotiator' => [
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($result instanceof Model && $result->hasErrors()){
            $this->ret    = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
        }

        $formatResult = [
            'ret'     =>  $this->ret,
            'errMsg'  =>  $this->errMsg,
            'data'    =>  parent::afterAction($action, $result),
        ];

        return $this->serializeData($formatResult);
    }

    public function  getTimeNow(){
        return time()*1000;
    }

    public function actionError(){
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
        $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
        return "$name: $message";
    }

    /*public function setHeaders(){
        $requestHeader = Yii::$app->request->getHeaders();
        $originUrl = isset($requestHeader['origin']) ? $requestHeader['origin'] : isset($requestHeader['refer']) ? $requestHeader['origin'] : '*';

        Yii::$app->response->getHeaders()
            ->set('Access-Control-Allow-Credentials', 'true')
            ->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, openToken')
            ->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
            ->set('Access-Control-Allow-Origin', $originUrl)
            ->set('Allow', 'POST, GET, PUT, DELETE, OPTIONS');
    }*/
}
