<?php
/**
 * User: ZengChao
 * Date: 2016/1/19
 * Time: 10:25
 */
namespace backend\controllers;

use backend\models\Source;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use backend\controllers\BaseController;
use backend\models\Locale;

class LocaleController extends BaseController
{
    public $modelClass = 'backend\models\Locale';

    public $scenario = Model::SCENARIO_DEFAULT;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['index', 'create', 'update', 'view', 'delete', 'base', 'all', 'parent'],
                    'rules' => [
                        [
                            'allow' => true,
                            'verbs' => ['OPTIONS']
                        ],
                        [
                            'actions' => ['index', 'create', 'update', 'view', 'delete', 'base', 'all', 'parent'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                    'denyCallback' => function ($rule, $action) { throw new UnauthorizedHttpException('You are requesting with an invalid credential.');},
                ]
            ]
        );
        return $behaviors;
    }

    public function actionCreate(){
        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->fallback = empty(Yii::$app->getRequest()->getBodyParam('fallback')) ? '1' : $model->fallback;
        $model->create_time = $this->getTimeNow();
        $model->modify_time = $this->getTimeNow();
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            //$id = implode(',', array_values($model->getPrimaryKey(true)));
            //$response->getHeaders()->set('Location', Url::toRoute(["view", 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionView($id){
        //$id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;
        $result = $modelClass::findOne($id);
        return $result;
    }

    public function actionUpdate($id){
        //$id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        $model->scenario = $this->scenario;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->fallback = empty(Yii::$app->getRequest()->getBodyParam('fallback')) ? '1' : $model->fallback;
        $model->modify_time = $this->getTimeNow();

        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

    public function actionDelete($id){
        //$id = Yii::$app->request->get('id');
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        return [];
    }

    /**
     * get all can-be-parent locales with id
     */
    public function actionParent(){
        $modelClass = $this->modelClass;
        return $modelClass::getAllParent();
    }

    public function actionAll(){
        $modelClass = $this->modelClass;
        return $modelClass::getAllLocale();
    }

    public function actionBase(){
        $modelClass = $this->modelClass;
        return [
            'language' => $modelClass::getBaseLanguage(),
            'country'  => $modelClass::getBaseCountry(),
        ];
    }

    public function prepareDataProvider()
    {
        $perPage = Yii::$app->request->get('pageSize');
        $attr = Yii::$app->request->get('attr');
        $sort = Yii::$app->request->get('sort');
        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::find();
        if(!empty($attr) && !empty($sort)){
            if(!in_array($attr, Locale::getAttributeName())){
                $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
                $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
                return;
            }

            if($sort == 'desc'){
                $query = $modelClass::find()->orderBy([$attr => SORT_DESC]);
            }else{
                $query = $modelClass::find()->orderBy([$attr => SORT_ASC]);
            }
        }
        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => empty($perPage) ? Yii::$app->params['default_page_size'] : $perPage,
                'pageSizeParam' => 'pageSize',
                'pageParam' => 'currentPage',
            ],
            'query' => $query,
        ]);
    }
}
