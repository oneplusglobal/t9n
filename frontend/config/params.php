<?php
return [
    'adminEmail' => 'admin@example.com',

    'auth' => [
        'magento' => '91a773716755fd23c8a4b862c5ba2769',
        'sdk' => 'eae18bc41e1434dd98fa2dd989531da8',

    ],

    'default_system' => 'T9N',

    'errCode' => [
        'dataValidateErr' => 0,
        'logicErr' => 2,
    ],
    'errMsg' => [
        'dataValidateErr' => 'invalid data',
        'logicErr' => 'not conform to design logic',
    ],
];
