<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/2/23
 * Time: 16:14
 */
namespace frontend\controllers;

use backend\models\Locale;
use Yii;
use backend\models\Source;
use backend\models\Translate;
use frontend\controllers\BaseController;

class SdkController extends BaseController
{
    public $modelClass = 'backend\models\Source';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['create'], $actions['delete'], $actions['view'], $actions['index']);
        return $actions;
    }

    protected function verbs()
    {
        return [
            'udall' => ['POST'],
            'udtime' => ['POST'],
            'require' => ['POST'],
        ];
    }

    public function actionUdtime(){
        return Translate::getLatestUpdateTime();
    }

    public function actionUdall(){
        return [
            "locale"    => Locale::sdkGetAll(),
            "translate" => Translate::sdkGetAll(),
        ];
    }

    public function actionRequire(){
        $translate = Translate::getByTagAndLocale($this->requestData['tag'], $this->requestData['language_id']);
        if($translate){
            return $translate;
        }

        if(Source::haveTag($this->requestData['tag'])){
            return false;
        }

        $model = new $this->modelClass();

        $model->load($this->requestData, '');
        $model->type = empty($model->type) ? Source::TYPE_UI : $model->type;
        $model->language_id = Locale::DEFAULT_SOURCE_LANGUAGE;
        $model->status = Source::STATUS_ACTIVE;
        $model->create_time = $this->getTimeNow();
        $model->modify_time = $this->getTimeNow();

        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return false;
    }
}