<?php
/**
 * Created by PhpStorm.
 * User: ZengChao
 * Date: 2016/2/23
 * Time: 11:48
 */
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\base\Exception;
use yii\web\HttpException;
use yii\base\UserException;
use yii\log\FileTarget;

class BaseController extends ActiveController
{
    public $ret = 1;
    public $errMsg = 'success';

    public $modelClass = '';

    public $requestData = '';

    public $serializer = [
        'class' => 'backend\models\Serializer',
        //'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        /*
         * set response header to application/json
         */
        return ArrayHelper::merge(parent::behaviors(), [
            'contentNegotiator' => [
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if(!parent::beforeAction($action)){
            return false;
        }

        $data = json_decode(Yii::$app->getRequest()->getBodyParam('data'), true);

        if(!is_array($data)){
            return false;
        }
        $this->requestData = $data['param'];
        if($this->validateData($data)){
            return true;
        }

        $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
        $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
        throw new HttpException(422, Yii::t('yii', 'invalid data'));
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($result instanceof Model && $result->hasErrors()){
            $this->ret    = Yii::$app->params['errCode']['dataValidateErr'];
            $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
        }

        $formatResult = [
            'ret'     =>  $this->ret,
            'errMsg'  =>  $this->errMsg,
            'data'    =>  parent::afterAction($action, $result),
        ];

        return $this->serializeData($formatResult);
    }

    public function  getTimeNow(){
        return time()*1000;
    }

    public function actionError(){
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        $this->ret = Yii::$app->params['errCode']['dataValidateErr'];
        $this->errMsg = Yii::$app->params['errMsg']['dataValidateErr'];
        return "$name: $message";
    }

    public function validateData($data){
        if(!isset(Yii::$app->params['auth'][$data['app']])){
            return false;
        }

        $validateData = [
            'param' => $data['param'],
            'key'   => Yii::$app->params['auth'][$data['app']],
        ];
        if(md5(serialize($validateData)) == $data['token']){
            return true;
        }
        return false;
    }
}
